# WBM - WebAssembly Bare Metal

This repository contains experimental code to create an enumlation of a bare metal hardware with WebAssembly running in a browser.

## General ideas & conclusion

The general idea is, that one creates a freestanding executable compiled to WebAssembly. Usually a freestanding executable contains something like `_start` function which does not return. This means an endless loop. This does not go well with the event loop in the browser. Therefore the idea was, to let this executable run in its own WebWorker but to use a SharedArrayBuffer in order to let the executable communicate with the "peripherals", think of memory mapped peripherals here. This means the executable would loop infinitely and just read and write specific elements of the SharedArrayBuffer. The idea would be to have a simple set-up to an embedded-like target - besides interrupts which I have no ideas how they could work inside the event loop of a WebWorker.

However this whole solution falls short, since when two or more WebWorkers read or write to the SharedArrayBuffer, then the result are only synchronized between the WebWorkers by sending messages between the WebWorkers (i.e., via `postMessage`) or by using synchronization via `Atomics` (e.g., `Atomics.notify` and `Atomics.wait`).

## server.py & GNU GPLv3

The server.py static server implementation is based on Francesco Pira's CORS enabled python http server: 

* [GitHub gist](https://gist.github.com/pirafrank/4089fd5532b4fdafac2bc3d476dd096e)
* [blog post](https://fpira.com/blog/2020/05/python-http-server-with-cors)

Francesco Pira's Code of his blog is published under GNU GPLv3. see [here](https://fpira.com/terms#license). This means the file `static/server.py` is also under GNU GPLv3

Therefore this project is licensed under [GPLv3](./LICENSE).

## Links

### WASM

* [https://github.com/rust-lang/rust/issues/52090]
* [https://www.hellorust.com/demos/import-memory/index.html]
* [https://radu-matei.com/blog/practical-guide-to-wasm-memory/]
* [https://github.com/ziglang/zig/issues/4866]
* [https://web.dev/webassembly-threads/]

### SharedArrayBuffer & WebWorker

* [https://stackoverflow.com/questions/70535752/enable-sharedarraybuffer-on-localhost/70536119#70536119]
* [https://blog.scottlogic.com/2019/07/15/multithreaded-webassembly.html]
* [https://developer.mozilla.org/en-US/docs/WebAssembly/JavaScript_interface/Module#sending_a_compiled_module_to_a_worker]
* [https://blog.logrocket.com/understanding-sharedarraybuffer-and-cross-origin-isolation/]
* [https://www.hongkiat.com/blog/shared-memory-in-javascript/]
* [https://reference.codeproject.com/javascript/reference/global_objects/sharedarraybuffer]
* [https://reference.codeproject.com/javascript/Reference/Global_Objects/Atomics]

### SharedArrayBuffer & WASM

* [https://ddr0.ca/blog-posts/13.Shared_Array_Buffers_With_WASM]

### Atomics

* [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Atomics]
* [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Atomics/wait]
* [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Atomics/waitAsync]

### Zig & WASM

* [https://blog.battlefy.com/zig-and-webassembly-are-a-match-made-in-heaven]
* [https://blog.battlefy.com/zig-made-it-easy-to-pass-strings-back-and-forth-with-webassembly]

### Cors

* [https://web.dev/cross-origin-isolation-guide/]
* [https://askubuntu.com/questions/1405271/how-do-i-enable-cors-for-a-local-python3-http-server]
* [https://fpira.com/blog/2020/05/python-http-server-with-cors]
* [https://necrashter.github.io/python-cross-origin-isolated-server]

