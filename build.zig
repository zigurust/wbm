const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const module_name = "wbm";
    const mode = b.standardReleaseOptions();
    // we want to create a wasm32 freestanding target by default
    const wasm_target = std.zig.CrossTarget{ .os_tag = .freestanding, .cpu_arch = .wasm32 };
    const target = b.standardTargetOptions(.{ .default_target = wasm_target });

    const exe = b.addExecutable(module_name, "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    // strip debug symbols
    //lib.strip = true; // uncomment if necessary
    exe.install();

    // copy artifact into static folder
    const path_to_module = "./zig-out/bin/" ++ module_name ++ ".wasm";
    const cp_cmd = b.addSystemCommand(&[_][]const u8{ "cp", path_to_module, "./static/" });
    b.getInstallStep().dependOn(&cp_cmd.step);

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const exe_tests = b.addTest("src/main.zig");
    exe_tests.setTarget(target);
    exe_tests.setBuildMode(mode);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&exe_tests.step);
}
