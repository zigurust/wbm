let memory = null;


onmessage = function(e) {
  console.log("Received message from main thread");
  memory = new Int32Array(e.data);
  console.log("onmessage hardware memory[0]", memory[0]);
  console.log("onmessage hardware memory[1]", memory[1]);
  postMessage("Initialized");
  setTimeout(loop, 250);
}

const loop = () => {
  console.log("loop memory[0]=", memory[0]);
  Atomics.add(memory,1,1);
  Atomics.notify(memory, 1, 1);
  console.log("loop memory[1]=", memory[1]);
}

