const hardware = new Worker("hardware.js");

const buffer = new SharedArrayBuffer(8);
const sharedMemory = new Int32Array(buffer); 
//;new WebAssembly.Memory({initial: 1, maximum: 1, shared: true});

sharedMemory[0] = 13;
sharedMemory[1] = 0;

hardware.postMessage(sharedMemory);

const valueInput = document.getElementById("value");

const changeValue = () => sharedMemory[0] = document.getElementById("value").value;

valueInput.onchange = changeValue;

hardware.onmessage = function(e) {
    console.log("Hardware status=", e.data);
    setTimeout(loop, 500);
}

const loop = async () => {
    console.log("index.js sharedMemory[1]=", sharedMemory[1]);
    const result = await Atomics.waitAsync(sharedMemory, 1, 0);
    console.log("index.js result=", result);
    result.value.then(() => console.log("awaited sharedMemory[1]", ssharedMemory[1]));
    //const atomic_wait = await result.value;
    //console.log("Atomic wait=", atomic_wait);
    console.log("index.js sharedMemory[1]=", sharedMemory[1]);
}

